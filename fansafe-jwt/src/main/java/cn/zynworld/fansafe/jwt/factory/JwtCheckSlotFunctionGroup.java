package cn.zynworld.fansafe.jwt.factory;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther Buynow Zhao
 * @create 2018/6/2
 * 维护一组 插槽检测方法
 */
public class JwtCheckSlotFunctionGroup {

	private List<JwtCheckSlotFunction> slotFunctionList = new ArrayList<>();


	public JwtCheckSlotFunctionGroup(List<JwtCheckSlotFunction> slotFunctionList) {
		this.slotFunctionList = slotFunctionList;
	}


	public JwtCheckSlotFunctionGroup() {}

	public JwtCheckSlotFunctionGroup addSlotFunction(JwtCheckSlotFunction slotFunction) {
		slotFunctionList.add(slotFunction);
		return this;
	}

	public List<JwtCheckSlotFunction> getSlotFunctionList() {
		return slotFunctionList;
	}

	public boolean isEmpty() {
		return slotFunctionList.isEmpty();
	}
}
