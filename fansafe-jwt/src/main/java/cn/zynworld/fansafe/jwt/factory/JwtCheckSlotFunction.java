package cn.zynworld.fansafe.jwt.factory;

import cn.zynworld.fansafe.jwt.core.JwtBean;
import jdk.nashorn.internal.objects.annotations.Function;

/**
 * @auther Buynow Zhao
 * @create 2018/6/2
 * 插槽检测，将插槽检测方法添加到{@link JwtBeanFactory}中，在检验过程中将触发检测
 */
@FunctionalInterface
public interface JwtCheckSlotFunction {
	boolean check(JwtBean jwtBean);
}
