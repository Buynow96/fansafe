package cn.zynworld.fansafe.jwt;

import cn.zynworld.fansafe.common.utils.StringUtils;
import cn.zynworld.fansafe.jwt.core.JwtBean;
import cn.zynworld.fansafe.jwt.factory.JwtBeanFactory;
import org.junit.Before;
import org.junit.Test;

/**
 * @auther Buynow Zhao
 * @create 2018/6/2
 */
public class JwtBeanTest {

	private final String SECREP = "FJKLSJFLKASJDKLFJfsdfs342,.";

	private JwtBeanFactory jwtBeanFactory;

	@Before
	public void init() {
		// init factory
		this.jwtBeanFactory = new JwtBeanFactory(SECREP);
		jwtBeanFactory.addCommonHead("common","公共 head");
		jwtBeanFactory.addCommonPlayload("common","公共 playload");
	}

	// 创建jwtBean
	public String testCreateJwtBean() {
		JwtBean jwtBean = jwtBeanFactory.newJwtBean();
		jwtBean.addHead("alg","HA256");
		jwtBean.addHead("type","jwt");

		jwtBean.addPlayload("username","buynow");
		jwtBean.addPlayload("userrole","admin");

		System.out.println(jwtBean.toJwtString());
		System.out.println(jwtBean);

		return jwtBean.toJwtString();
	}

	// 对正确的jwt字符串进行验证 并读取数据
	@Test
	public void testCheckTrueJwtBean() {
		String jwtString = testCreateJwtBean();
		JwtBean jwtBean = jwtBeanFactory.getJwtBean(jwtString);
		System.out.println(jwtBean.getHead("common"));
		System.out.println(jwtBean.getHead("type"));
		System.out.println(jwtBean.getPlayload("common"));
		System.out.println(jwtBean.getPlayload("username"));
	}


	// 对错误的jwt字符串进行验证
	@Test
	public void testCheckErrorJwtBean() {
		// 对正确的jwt字符串进行修改
		String jwtString = testCreateJwtBean()+"a";
		// 生成jwtBean
		JwtBean jwtBean = jwtBeanFactory.getJwtBean(jwtString);
		// 因为是错误的 验证不通过 将返回null
		System.out.println(jwtBean == null);
	}


	// 插槽方法的使用
	@Test
	public void testSlotCheckFun() {
		// init factory
		this.jwtBeanFactory = new JwtBeanFactory(SECREP);
		// add slotFun 添加一个身份过滤的插槽方法
		jwtBeanFactory.addSlotFunction(jwtBean -> {
			if (jwtBean == null) {
				return false;
			}
			String role = jwtBean.getPlayload("role",String.class);
			if (StringUtils.isEmpty(role)) {
				return false;
			}
			if (role.equals("admin")) {
				return true;
			}
			return false;
		});
		// 检验role 为 admin 情况
		JwtBean jwtBean = jwtBeanFactory.newJwtBean();
		String jwtString = jwtBean.addPlayload("role", "admin").toJwtString();
		jwtBean = jwtBeanFactory.getJwtBean(jwtString);
		System.out.println(jwtBean); // 结果检验成功

		// 检验role 部位admin 情况
		jwtBean = jwtBeanFactory.newJwtBean();
		jwtString = jwtBean.addPlayload("role", "none").toJwtString();
		jwtBean = jwtBeanFactory.getJwtBean(jwtString);
		System.out.println(jwtBean); // 因为插槽方法过滤 结果检验失败 返回null

	}
}
