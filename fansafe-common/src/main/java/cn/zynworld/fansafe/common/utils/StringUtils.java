package cn.zynworld.fansafe.common.utils;

/**
 * @auther Buynow Zhao
 * @create 2018/6/2
 */
public class StringUtils {

	public static boolean isEmpty(String str) {
		return str == null || "".equals(str);
	}
}
